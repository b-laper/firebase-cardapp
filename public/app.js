// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";
import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-analytics.js";

import {
  getFirestore,
  onSnapshot,
  updateDoc,
  getDoc,
  getDocs,
  setDoc,
  doc,
  collection,
  orderBy,
  limit,
  query,
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-firestore.js";

import {
  getAuth,
  signInWithEmailAndPassword,
  createUserWithEmailAndPassword,
  signOut,
  onAuthStateChanged,
  AuthErrorCodes,
  GoogleAuthProvider,

  // signInWithPopup,
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";

import {
  getDatabase,
  set,
  ref as databaseRef,
  onValue,
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBwYNqcIsP8Hp4MCGkeR8MTTd0naMyQvAU",
  authDomain: "cardshop-3959d.firebaseapp.com",
  projectId: "cardshop-3959d",
  databaseURL:
    "https://cardshop-3959d-default-rtdb.europe-west1.firebasedatabase.app",
  storageBucket: "cardshop-3959d.appspot.com",
  messagingSenderId: "397174964234",
  appId: "1:397174964234:web:dcb15b6357de775a04dff3",
  measurementId: "G-TFG6Q4D029",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
const db = getFirestore(app);
// Connect auth
const auth = getAuth(app);
const provider = new GoogleAuthProvider();
//Connect Real Time DB

const mainSelector = {
  divCards: document.querySelector("#cardsDiv"),
  containerDiv: document.querySelector("#container"),
};

const logInSelectors = {
  logIn: document.querySelector("#logIn"),
  signUp: document.querySelector("#signUp"),
  registerButton: document.querySelector("#registerButton"),
  emailInput: document.querySelector("#emailInput"),
  passwordInput: document.querySelector("#passwordInput"),
  logInBox: document.querySelector("#logInBox"),
  errorMessage: document.querySelector("#errorMessage"),
  logInDiv: document.querySelector("#logInDiv"),
  signUpDiv: document.querySelector("#signUpDiv"),
};

const userSelectors = {
  userPanelDiv: document.querySelector("#userPanelDiv"),
  logOutDiv: document.querySelector("#logOutDiv"),
  userAccountImg: document.querySelector("#userAccountImg"),
  logOutImg: document.querySelector("#logOutImg"),
  countryList: document.querySelector("#countryList"),
  userAccountBox: document.querySelector("#userAccountBox"),
  verifyInfo: document.querySelector("#verifyInfo"),
};

const userDetailsSelectors = {
  name: document.querySelector("#name"),
  lastName: document.querySelector("#lastName"),
  address: document.querySelector("#address"),
  street: document.querySelector("#street"),
  countryList: document.querySelector("#countryList"),
  attachedID: document.querySelector("#attachedID"),
  confirm: document.querySelector("#confirm"),
  IdHandlerInput: document.querySelector("#IdHandler"),
  divAttachID: document.querySelector("#attachID"),
};

const shopSelectors = {
  shopImg: document.querySelector("#shopImg"),
  shopBox: document.querySelector("#shopBox"),
  addToCartButton: document.querySelectorAll(".addToCart"),
};
const accountBalanceSelectors = {
  userMoneyBalance: document.querySelector("#userMoneyBalance"),
  moneyBalance: document.querySelector("#moneyBalance"),
  moneyImg: document.querySelector("#moneyImg"),
  userInfoDeposit: document.querySelector("#userInfoDeposit"),
};

logInSelectors.logIn.addEventListener("click", () => {
  toggleStyles(document.querySelector("#cardsDiv"), "none");
  toggleStyles(logInSelectors.logInBox, "flex");
  logInSelectors.registerButton.innerHTML = "Log In";
  logInSelectors.passwordInput.placeholder = "Type your password";
  logInSelectors.registerButton.addEventListener("click", logInHandler);
});

logInSelectors.signUp.addEventListener("click", () => {
  mainSelector.divCards.innerHTML = "";
  toggleStyles(logInSelectors.logInBox);
  logInSelectors.registerButton.innerHTML = "Register";
  logInSelectors.passwordInput.placeholder = "Create password min.6 characters";
  logInSelectors.registerButton.addEventListener("click", signUpHandler);
});

const sortSelectors = {
  priceSortAsc: document.querySelector("#priceSortAsc"),
  priceSortDesc: document.querySelector("#priceSortDesc"),
  limitSort: document.querySelector("#limitSort"),
  numSort: document.querySelector("#numSort"),
  nameSort: document.querySelector("#nameSort"),
};

const toggleStyles = (selector, style = "flex") => {
  selector.style.display = style;
};

const isLoggedIn = (txt) => {
  logInSelectors.logInBox.innerHTML = `<h4>${txt}</h4>`;
  setTimeout(() => {
    toggleStyles(logInSelectors.logInBox, "none");
  }, 2000);
};

const errorTxtCleaner = (time = 3000) => {
  logInSelectors.errorMessage.style.color = "red";
  setTimeout(() => (logInSelectors.errorMessage.innerText = ""), time);
};

const logInHandler = async () => {
  const email = logInSelectors.emailInput.value;
  const password = logInSelectors.passwordInput.value;
  try {
    const user = await signInWithEmailAndPassword(auth, email, password);
    isLoggedIn("Logged in sucessfully");
    window.location.reload();
  } catch (error) {
    console.log(error);
    if (
      logInSelectors.emailInput.value.length < 1 ||
      logInSelectors.passwordInput.value.length < 1
    ) {
      logInSelectors.errorMessage.innerText = "Fields are required";
      errorTxtCleaner(5000);
    } else if (error.code === AuthErrorCodes.INVALID_PASSWORD) {
      logInSelectors.errorMessage.innerText = "Invalid password";
      errorTxtCleaner();
    } else if (error.code) {
      logInSelectors.errorMessage.innerText = "Invalid email";
      errorTxtCleaner();
    }
  }
};

const signUpHandler = async () => {
  const email = logInSelectors.emailInput.value;
  const password = logInSelectors.passwordInput.value;
  try {
    const user = await createUserWithEmailAndPassword(auth, email, password);
    isLoggedIn("User created sucessfully and logged in");
  } catch (error) {
    console.log(error);

    if (error.code) {
      logInSelectors.errorMessage.innerText = "User not created";
      errorTxtCleaner();
    }
  }
};

const countriesEU = [
  "Austria",
  "Belgium",
  "Bulgaria",
  "Cyprus",
  "Czech Republic",
  "Germany",
  "Denmark",
  "Estonia",
  "Spain",
  "Finland",
  "France",
  "United Kingdom",
  "Greece",
  "Hungary",
  "Croatia",
  "Ireland, Republic of (EIRE)",
  "Italy",
  "Lithuania",
  "Luxembourg",
  "Latvia",
  "Malta",
  "Netherlands",
  "Poland",
  "Portugal",
  "Romania",
  "Sweden",
  "Slovenia",
  "Slovakia",
];

countriesEU.forEach((country) => {
  userSelectors.countryList.innerHTML += `<option value=${country}>${country}</option>`;
});

const logoutUser = async () => {
  await signOut(auth);
  console.log("Log out sucessfully");
  toggleStyles(userDetailsSelectors.divAttachID, "none");
  toggleStyles(userSelectors.userAccountBox, "none");
  shopSelectors.shopBox.innerHTML = "";
  isAuth();
};
userSelectors.logOutImg.addEventListener("click", logoutUser);
// auth actions
const isAuth = async () => {
  onAuthStateChanged(auth, (user) => {
    if (user) {
      toggleStyles(logInSelectors.logInDiv, "none");
      toggleStyles(logInSelectors.signUpDiv, "none");
      toggleStyles(userSelectors.logOutDiv);
      toggleStyles(userSelectors.userPanelDiv, "flex");
    } else {
      toggleStyles(userDetailsSelectors.divAttachID, "none");
      toggleStyles(logInSelectors.logInDiv, "inline");
      toggleStyles(logInSelectors.signUpDiv, "inline");
      toggleStyles(userSelectors.userPanelDiv, "none");
      toggleStyles(userSelectors.logOutDiv, "none");
    }
  });
};
// user account settings listener
userSelectors.userAccountImg.addEventListener("click", () => {
  if (userSelectors.userAccountBox.style.display === "none") {
    mainSelector.divCards.innerHTML = "";
    toggleStyles(userSelectors.userAccountBox, "flex");
  } else {
    toggleStyles(userSelectors.userAccountBox, "none");
    renderCards();
  }
});

isAuth();
//send user Settings to DB
const users = "users";

// If user uid not exist in DB create form to add verification form
const createUserFormToSendToDataBase = (userUid, userEmail) => {
  const user = {
    email: userEmail,
    Adress: userDetailsSelectors.address.value,
    Country: userDetailsSelectors.countryList.value,
    Street: userDetailsSelectors.street.value,
    idImg: "",
    lastName: userDetailsSelectors.lastName.value,
    name: userDetailsSelectors.name.value,
    accountBalance: 0,
  };
  const newUserRef = doc(db, users, `${userUid}`);
  console.log(user);
  if (
    userUid.length > 2 &&
    userEmail.length > 2 &&
    user.Adress.length > 2 &&
    user.Country.length > 2 &&
    user.Street.length > 2 &&
    user.lastName.length > 2 &&
    user.name.length > 2
  ) {
    setDoc(newUserRef, user);
    console.log("data");
    userSelectors.userAccountBox.innerHTML = `<h4>Data sent sucessfully!</h4>`;
    setTimeout(() => {
      window.location.reload();
    }, 2000);
  } else {
    userSelectors.userAccountBox.innerHTML += `<h4 style="color:red">All Fields must be valid!</h4>`;
  }
};

onAuthStateChanged(auth, async (userData) => {
  if (userData) {
    const docRef = doc(db, users, userData.uid);
    const docSnap = await getDoc(docRef);
    if (docSnap.exists()) {
      const {
        _document: {
          data: {
            value: {
              mapValue: {
                fields: {
                  accountBalance: { integerValue },
                  idImg: { stringValue },
                },
              },
            },
          },
        },
      } = docSnap;
      if (stringValue === "") {
        userDetailsSelectors.IdHandlerInput.addEventListener(
          "change",
          async (e) => {
            const userUpdateRef = doc(db, users, userData.uid);
            const field = {
              idImg: `${e.target.files[0].name}`,
            };
            //later on I will add function adding image to DB storage***
            await updateDoc(userUpdateRef, field);
            window.location.reload();
          }
        );
      } else {
        toggleStyles(userDetailsSelectors.divAttachID, "none");
      }
      const amount = docSnap.exists() ? integerValue : 0;
      accountBalanceSelectors.moneyImg.addEventListener("click", (e) => {
        if (document.querySelector("#moneyDeposit")) {
          document.querySelector("#moneyDeposit").remove();
        } else {
          createDepositWindow();
          const depositAmount = document.querySelector("#depositAmount");
          document
            .querySelector("#depositButton")
            .addEventListener("click", () => {
              depositMoney(
                userData.uid,
                parseInt(amount),
                parseInt(depositAmount.value)
              );

              window.location.reload();
            });
        }
      });
      accountBalanceSelectors.userMoneyBalance.innerHTML = docSnap.exists()
        ? integerValue
        : 0;
      userSelectors.userAccountImg.addEventListener("click", () => {
        userSelectors.userAccountBox.innerHTML = "";
        Object.keys(docSnap.data()).forEach((key) => {
          userSelectors.userAccountBox.innerHTML += `<p>${key}: ${
            docSnap.data()[key]
          }</p>`;
        });
      });
    } else {
      userSelectors.verifyInfo.innerHTML = "Verify account form";
      accountBalanceSelectors.userInfoDeposit.innerText = "User not verified";
      toggleStyles(accountBalanceSelectors.moneyImg, "none");
      userSelectors.userAccountImg.addEventListener("click", () => {
        userDetailsSelectors.confirm.addEventListener("click", () => {
          if (auth) {
            createUserFormToSendToDataBase(userData.uid, userData.email);
          }
        });
      });
    }
  }
});

//fetch cardlist from database

const cards = "cards";
const allAvaibleCards = collection(db, cards);

const renderCards = async () => {
  const querySnapshot = await getDocs(allAvaibleCards);
  const q = query(allAvaibleCards, orderBy("price", "desc"));
  querySnapshot.forEach((doc) => {
    mainSelector.divCards.innerHTML += `
  <div class="card">
    <h5>${doc.id}</h5>
    <p class="cardPrice">Buy for <span>${doc.data().price}</span> € </p>
    <button class="addToCart button" type="button">Add to Cart</button>
    <img class="cardImg" src="${doc.data().img}" alt="${doc.id}" />
  </div>`;
    mainSelector.containerDiv.style.height = "auto";
  });

  const buttons = document.querySelectorAll(".addToCart");
  buttons.forEach((button) => {
    button.addEventListener("click", (e) => {
      const itemName = e.target.parentNode.firstChild.nextSibling.innerText;
      const prize = e.target.previousElementSibling.childNodes[1].innerText;
      window.localStorage.setItem(itemName, prize);
    });
  });
};

renderCards();

// shopping cart local storage

let total = 0;

shopSelectors.shopImg.addEventListener("click", () => {
  buy.innerHTML = "";
  let arr = [];
  if (shopSelectors.shopBox.innerHTML.length > 4) {
    shopSelectors.shopBox.innerHTML = "";
  } else {
    shopSelectors.shopBox.innerHTML = "Shopping Cart:";
    function forEachKey() {
      for (let i = 0; i < localStorage.length; i++) {
        arr.push(localStorage.key(i));
      }

      arr.forEach((item, index) => {
        total += parseInt(localStorage.getItem(item));
        shopSelectors.shopBox.innerHTML += `
        <p class="shopItem">${arr[index]}</p>
        <span>${localStorage.getItem(item)}</span>
        <button class="removeFromCart button" type="button" >Remove card from cart</button>
        `;
      });
      const removeButtons = document.querySelectorAll(".removeFromCart");
      removeButtons.forEach((button) => {
        button.addEventListener("click", (e) => {
          const key =
            e.target.previousElementSibling.previousElementSibling.innerText;
          removeItemFromShoppingCart(key);
        });
      });
    }
    forEachKey();
    if (Object.keys(localStorage).length >= 1) {
      buttonsShow();
    }
  }
});

const buttonsShow = () => {
  onAuthStateChanged(auth, async (userData) => {
    if (userData) {
      const docRef = doc(db, users, userData.uid);
      const docSnap = await getDoc(docRef);
      if (docSnap.exists()) {
        const {
          _document: {
            data: {
              value: {
                mapValue: {
                  fields: {
                    accountBalance: { integerValue },
                    idImg: { stringValue },
                  },
                },
              },
            },
          },
        } = docSnap;
        const span = document.createElement("span");
        const buy = document.querySelector("#buy");
        if (stringValue) {
          buy.innerHTML += `<div id="shopButtons">
        <button id="buyNow" type="button" class="button">Buy Now</button>
        <button id="clearButton" type="button" class="button">Clear</button>
        </div>`;
          const buyNowButton = document.querySelector("#buyNow");
          const clearButton = document.querySelector("#clearButton");
          buyNowButton.addEventListener("click", async () => {
            if (integerValue >= total) {
              let sum = integerValue - total;
              const field = {
                accountBalance: sum,
              };
              await updateDoc(docRef, field);
              shopSelectors.shopBox.appendChild(span).innerText =
                "Cards will be shipped soon!";
              removeAllItemsFromShoppingCart();
              setTimeout(() => {
                window.location.reload();
              }, 2000);
            } else {
              shopSelectors.shopBox.appendChild(span).innerText =
                "Deposit more money!";
            }
          });
          clearButton.addEventListener("click", removeAllItemsFromShoppingCart);
        }
      }
    }
  });
};
const removeItemFromShoppingCart = (key) => {
  shopSelectors.shopBox.innerHTML = "Shopping cart:";
  localStorage.removeItem(key);
};

const removeAllItemsFromShoppingCart = () => {
  shopSelectors.shopBox.innerHTML = "Shopping cart:";
  Object.keys(localStorage).forEach((key) => {
    localStorage.removeItem(key);
  });
};
const depositMoney = async (userUid, value, balance) => {
  const userUpdateRef = doc(db, users, userUid);
  const field = {
    accountBalance: balance + value,
  };
  await updateDoc(userUpdateRef, field);
};

const createDepositWindow = () => {
  const div = document.createElement("div");
  div.id = "moneyDeposit";
  userSelectors.logOutDiv.appendChild(div);
  div.innerHTML = `

<label for="">Amount</label>
<input type="number" id="depositAmount" min=100 max=10000 placeholder="Set amount in EUR min.100 max.10000">
<button class="button" id="depositButton" type="button">Transfer</button>`;
};

const sortedRender = (arr) => {
  mainSelector.divCards.innerHTML = "";
  arr.forEach((doc) => {
    mainSelector.divCards.innerHTML += ` <div class="card">
    <h5>${doc.id}</h5>
    <p class="cardPrice">Buy for <span>${doc.price}</span> € </p>
    <button class="addToCart button" type="button">Add to Cart</button>
    <img class="cardImg" src="${doc.img}" alt="${doc.id}" />
  </div>`;
  });
  mainSelector.containerDiv.style.height = "auto";
};
const sortByPriceAsc = () => {
  mainSelector.divCards.innerHTML = "";
  const q = query(allAvaibleCards, orderBy("price"));
  onSnapshot(q, (snapshot) => {
    let cardsAsc = [];
    snapshot.docs.forEach((doc) => {
      cardsAsc.push({ ...doc.data(), id: doc.id });
    });
    sortedRender(cardsAsc);
  });
};
const sortByPriceDesc = () => {
  mainSelector.divCards.innerHTML = "";
  onSnapshot(query(allAvaibleCards, orderBy("price", "desc")), (snapshot) => {
    let cardsDesc = [];
    snapshot.docs.forEach((doc) => {
      cardsDesc.push({ ...doc.data(), id: doc.id });
      sortedRender(cardsDesc);
    });
  });
};

const sortByNumber = (e) => {
  mainSelector.divCards.innerHTML = "";
  onSnapshot(
    query(allAvaibleCards, orderBy("price"), limit(e.target.value)),
    (snapshot) => {
      let cardsArr = [];
      snapshot.docs.forEach((doc) => {
        cardsArr.push({ ...doc.data(), id: doc.id });
        sortedRender(cardsArr);
      });
    }
  );
};

sortSelectors.priceSortAsc.addEventListener("click", sortByPriceAsc);
sortSelectors.priceSortDesc.addEventListener("click", sortByPriceDesc);
sortSelectors.numSort.addEventListener("change", (e) => {
  console.log(e.target.value);
  sortByNumber(e);
});
sortSelectors.nameSort.addEventListener("click", () => {
  mainSelector.divCards.innerHTML = "";
  renderCards();
});

const realtimeDatabase = getDatabase();

const chatHandler = () => {
  onAuthStateChanged(auth, async (userData) => {
    const date = new Date();

    console.log(date);
    chatSelectors.chat.innerHTML = "";
    if (auth) {
      await set(databaseRef(realtimeDatabase, `${date}chat/`), {
        message: chatSelectors.chatMessage.value,
        user: userData.email,
      });
      console.log(chatSelectors.chatMessage.value, userData.email);
    }
    chatSelectors.chatMessage.value = "";
  });
};

const chatSelectors = {
  chat: document.querySelector("#chat"),
  chatMessage: document.querySelector("#chatMessage"),
  chatButton: document.querySelector("#chatButton"),
  showOrHideChat: document.querySelector("#showChat"),
  chatBox: document.querySelector("#chatBox"),
};

const getMessagesFromChatInDB = async () => {
  chatSelectors.chat.innerHTML = "";
  const ref = databaseRef(realtimeDatabase);

  await onValue(ref, (snapshot) => {
    const fullData = snapshot.val();
    console.log(fullData);
    Object.values(fullData).forEach((post) => {
      console.log(post);
      chatSelectors.chat.innerHTML += `
      <h5>${post.user}</h5>
      <p>${post.message}</p>
      `;
    });
  });
};
chatSelectors.chatMessage.addEventListener("keydown", (e) => {
  if (e.key == "Enter") {
    chatHandler(e);
  }
});
chatSelectors.chatButton.addEventListener("click", (e) => {
  console.log("click");
  chatHandler(e);
});
getMessagesFromChatInDB();

chatSelectors.showOrHideChat.addEventListener("click", () => {
  if ((chatSelectors.chatBox.style.display = "none"))
    toggleStyles(chatSelectors.chatBox, "block");
});
